<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1" isELIgnored="false"%>
  <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
</head>
<body>
<H1>Menu Item</H1>

 <table  cellpadding="2">  
<tr><th>Name</th><th>Category</th><th>Price</th><th>Status</th><th>Date Of Launch</th><th>Free Delivery</th><th>Edit</th></tr>  
   <c:forEach var="item" items="${itemList}">   
   <tr>  
   <td>${item.name}</td>  
   <td>${item.category}</td>  
   <td>${item.price}</td>  
   <td>${item.status}</td> 
   <td>${item.dateofLauch}</td>
   <td>${item.freeDelivery}</td> 
   <td><a href="edititem/${item.name}">Edit</a></td>  
    </tr>  
   </c:forEach>  
   </table>   

</body>
</html>