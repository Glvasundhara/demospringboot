<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>

<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1" isELIgnored="false"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
</head>
<body>
    ${msg}
	<h1>Login Page</h1>
 	<form:form id="loginForm" method="post" action="login" modelAttribute="loginBean">
 
            username<form:input path="user" id="username" name="user" />
		<br>
            
            Password <form:password path="password" id="password" name="password" />
		<br>
		<input type="submit" value="Submit" />
	</form:form>

</body>
</html>