<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
</head>
<body>
	<H2>Registration Form</H2>
	<form:form id="registerForm" method="post" action="register" modelAttribute="rBean">
 
           Name<form:input path="name" />
		<br>
             
            City <form:select path="city" items="${cityList}" />
		<br>
		<input type="submit" value="Submit" />
	</form:form>


</body>
</html>