package com.cognizant.demospringboot;

public class MenuItem {
  private String name;
  private float price;
  private String category;
  private String status;
  private String dateofLauch;
  private String freeDelivery;
public MenuItem() {
	super();
}
public String getName() {
	return name;
}
public void setName(String name) {
	this.name = name;
}
public float getPrice() {
	return price;
}
public void setPrice(float price) {
	this.price = price;
}
public String getCategory() {
	return category;
}
public void setCategory(String category) {
	this.category = category;
}
public String getStatus() {
	return status;
}
public void setStatus(String status) {
	this.status = status;
}
public String getDateofLauch() {
	return dateofLauch;
}
public void setDateofLauch(String dateofLauch) {
	this.dateofLauch = dateofLauch;
}
public String getFreeDelivery() {
	return freeDelivery;
}
public void setFreeDelivery(String freeDelivery) {
	this.freeDelivery = freeDelivery;
}
  
  
}
