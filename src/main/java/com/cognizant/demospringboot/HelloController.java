package com.cognizant.demospringboot;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletContext;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.web.servlet.RegistrationBean;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;




@Controller
public class HelloController {
	
	@Autowired
	ItemDao itemDao;
	
    @RequestMapping("/hello")
    @ResponseBody	
    public String sayHello() {
		return "Hi there";
	}

	/*
	 * @RequestMapping("/login")
	 *  public String getLoginForm() { return "login"; }
	 */    
    @RequestMapping(value="/login",method=RequestMethod.GET)
    public ModelAndView getLoginPage() {
    	ModelAndView model = new ModelAndView("login");
        LoginBean loginBean1 = new LoginBean();
        model.addObject("loginBean", loginBean1);
        return model;
    	//return "login";
	}
    
    
	/*
	 * @RequestMapping(value="/login",method=RequestMethod.POST) public String
	 * validateLogin(@ModelAttribute("loginBean")LoginBean loginBean,ModelMap mv) {
	 * 
	 * String viewName=null; if(loginBean.getUser().equals("admin") &&
	 * loginBean.getPassword().equals("admin")) {
	 * 
	 * mv.addAttribute("x",loginBean.getUser()); viewName="welcome"; }else {
	 * 
	 * 
	 * mv.addAttribute("msg","invalid username/password"); viewName="login"; }
	 * return viewName; }
	 */
    
    @RequestMapping(value="/login",method=RequestMethod.POST)
    public ModelAndView validateLogin(@ModelAttribute("loginBean")LoginBean loginBean) {
    	ModelAndView mv=null; 
    	if(loginBean.getUser().equals("admin") && loginBean.getPassword().equals("admin")) {
    		mv= new ModelAndView("welcome");
    		mv.addObject("x",loginBean.getUser());
    	
    	}else {
    		
    		mv= new ModelAndView("login");
    		mv.addObject("msg","invalid username/password");
    	    loginBean.setUser("");
    	}
    	return mv;
	}
    @RequestMapping(value="/register",method=RequestMethod.GET)
   
    public ModelAndView getRegForm() {
    	RegisterBean rb= new RegisterBean();
    	ModelAndView mv=new ModelAndView("register");
    	List<String> cList= new ArrayList();
    	cList.add("Pune");
    	cList.add("Mumbai");
    	cList.add("Delhi");
    	mv.addObject("rBean",rb);
    	mv.addObject("cityList",cList);
    	return mv;
    }
    
    @RequestMapping(value="/register",method=RequestMethod.POST)
    
    public ModelAndView successRegistration(@ModelAttribute("rBean")RegisterBean z) {
    	ModelAndView mv=new ModelAndView("success_reg"); 
    	mv.addObject("xBean",z);
    	return mv;
	}
    @RequestMapping(value="/getmenu",method=RequestMethod.GET)
    public ModelAndView getMenu() {
    	ModelAndView mv= new ModelAndView("menu_item_list");
         List<MenuItem>aList  = itemDao.getMItems();
    	//ArrayList<MenuItem>aList= new ArrayList<MenuItem>();
    	/*MenuItem menuItem=new MenuItem();
    	menuItem.setName("Pen");
    	menuItem.setCategory("Stationary");
    	menuItem.setDateofLauch("12/4/2020");
    	menuItem.setFreeDelivery("yes");
    	menuItem.setPrice(20);
    	menuItem.setStatus("YES");
    	MenuItem menuItem1=new MenuItem();
    	menuItem1.setName("Pencil");
    	menuItem1.setCategory("Stationary");
    	menuItem1.setDateofLauch("13/4/2020");
    	menuItem1.setFreeDelivery("yes");
    	menuItem1.setPrice(10);
    	menuItem1.setStatus("YES");
    	aList.add(menuItem);
    	aList.add(menuItem1);*/
    	mv.addObject("itemList",aList);
    	return mv;
    }
    
    @RequestMapping(value="/edititem/{m_name}")    
    public ModelAndView edit(@PathVariable("m_name") String name,@ModelAttribute("menu") MenuItem menu){    
        ModelAndView mv= new ModelAndView("edit_item");
        System.out.println(name);
    	menu=itemDao.getMenuItem(name);
        System.out.println("in edit");
        mv.addObject("menu",menu); 
        System.out.println(menu.getCategory());
        return mv;    
    }  
    @RequestMapping(value="/updateitem",method=RequestMethod.POST)
    public String update(@ModelAttribute("menu") MenuItem x) {
    	System.out.println(" om update method");
    	ModelAndView mv= new ModelAndView("update_success");
    	itemDao.update(x);
    	return "update_success";
    }
}
