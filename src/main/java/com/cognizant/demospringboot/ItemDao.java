package com.cognizant.demospringboot;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;




@Repository
public class ItemDao {

	@Autowired
    private JdbcTemplate jdbcTemplate;
	
	public List<MenuItem> getMItems() {
		 List<MenuItem> mList= new ArrayList<MenuItem>();
		 List<Map<String, Object>> rowList = jdbcTemplate.queryForList("select * from menuitem");
		 for (Map<String, Object> map : rowList) 
         {
              MenuItem m = new MenuItem();
              
              m.setName((String)map.get("name"));
              m.setCategory((String)map.get("category"));
              m.setPrice((int)map.get("price"));
              m.setDateofLauch((String)map.get("dateoflaunch"));
              m.setFreeDelivery((String)map.get("freedelivery"));
              m.setStatus((String)map.get("status"));
              mList.add(m);
          }
		
		return mList;
	} 
	public MenuItem getMenuItem(String name) {
		String sql="select * from menuitem where name='"+name+"'";
		System.out.println(sql);
		List <MenuItem>mlist= jdbcTemplate.query(sql,new ItemRowMapper());
	    System.out.println(mlist.size());
	    MenuItem m=mlist.get(0);
	    return m; 
		
	}
	
	class ItemRowMapper implements RowMapper<MenuItem>{
		@Override
		public MenuItem mapRow(ResultSet rs, int rowNum) throws SQLException {			
			    
			    	MenuItem e=new MenuItem(); 
			    	e.setName(rs.getString("name"));
			        e.setCategory(rs.getString("category"));
			        e.setPrice(rs.getInt("price"));
			        e.setDateofLauch(rs.getString("dateoflaunch")); 
			        e.setFreeDelivery(rs.getString("freedelivery"));
			        e.setStatus(rs.getString("status"));
			        System.out.println(" in <Mapper");
			        return e; 
			    
			 	
		}//
	}

	public int update(MenuItem itemBean) {
      String m_name=itemBean.getName();
      String m_category=itemBean.getCategory();
      float m_price =itemBean.getPrice(); 
      String m_dateOfLaunch=itemBean.getDateofLauch();
	  String m_status=itemBean.getStatus();
	  String m_freeDelivery=itemBean.getFreeDelivery();
       String query="update menuitem set name='"+m_name+"' , price = "+m_price+" , category ='"+m_category +"' where name='"+m_name+"'" ;
	   int rec= jdbcTemplate.update(query);
	   return rec;
	}


}
